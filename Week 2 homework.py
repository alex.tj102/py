
# coding: utf-8

# # Homework

# - Write code to print out 1 to 100. If the number is a multiple of 3, print out "fizz" instead of the number. If the number is a multiple of 5, print out "buzz". If the number is multiple of 3 and 5, print out "fizzbuzz".

# In[31]:


a.


# In[43]:


for i in range(101):
    if i<5:
        print(i)
    elif (i%5 == 0 and i%3 == 0):
        print('fizzbuzz')
    elif i%3 == 0:
        print('fizz')
    elif i%5 == 0:
        print('buzz')
    else:
        print(i)


# - Let’s say I give you a list saved in a variable: a = [[1, 4], [9, 16], [25, 36]]. Write one line of Python that takes this list a and makes a new list that contains [1, 4, 9, 16, 25, 36]

# In[69]:


a = [[1, 4], [9, 16], [25, 36]]
def new_list():
    b = a[0]
    c = a[1]
    d = a[2]
    return(b + c + d)
my_result = new_list()
my_result


# In[73]:


b = a[0]
c = a[1]
d = a[2]
e = b + c + d
e


# - Given a dictionary my_dict = {'a': 9, 'b': 1, 'c': 12, 'd': 7}. Write code to print out a list of sorted key based on their value. For example, in this case, the code should print out ['b', 'd', 'a', 'c']

# In[106]:


my_dict = {'a': 9, 'b': 1, 'c': 12, 'd': 7}


# In[111]:


import operator
sorted_my_dict = sorted(my_dict.items(), key=operator.itemgetter(1))
sorted_my_dict


# - Take two lists, say for example these two:
# 
# 	a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
#     
#     b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
#     
# and write a program that returns a list that contains only the elements that are common between the lists (without duplicates). Make sure your program works on two lists of different sizes. 

# In[129]:


a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
a.append(0)
a.insert(-1,0)
a


# In[138]:


a1 = set(a)
b1 = set(b)
c = list(a1.intersection(b1))
print(c)


# - Write a function that accept a string as a single argument and print out whether that string is a palindrome. (A palindrome is a string that reads the same forwards and backwards.) For example, "abcddcba" is a palindrome, "1221" is also a palindrome.

# In[160]:


a = 1221
type(a)


# - You are given a string and width W. Write code to wrap the string into a paragraph of width W.

# In[145]:


a = """This is the string I'm given. 
I have to translate this tring into a paragraph"""
len(a)


# In[157]:


import textwrap

# Wrap this text. 
wrapper = textwrap.TextWrapper(width=50) 
word_list = wrapper.wrap(text=a) 
  
# Print each line. 
for element in word_list: 
    print(element) 


# In[158]:


len(element)


# In[159]:


type(element)

